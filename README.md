**Introduction**

The project is about automating test validation of diffrent API's usind diffrent methods like get, post, put, patch and delete in postman software.
API's which are used to perform test scripts are refferd form reqres.in website.

**Features**

In this project i have used postman software to automate the test validation of REST API to test diffrent request methods of the api to check wether it is working correctly or not.

Also used data driven testing by sendibg request with excel format and with json format and triyed diffrent number of iteration buy running Collection.

Created enviornment variables and global variables which are commonly used in all api to save time and avoide mistakes in repeatable tasks.

Also generated test reports by using Newman which is easily understandable to all, reports are in html format which can be easily open by using any browser.
